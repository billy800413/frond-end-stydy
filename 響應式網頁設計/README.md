# 六角學園的課程筆記

## 響應式網頁設計

### 基本介紹
#### Responsive Web Design 基礎- html head 裡的 viewport
* 詳細[參考](https://medium.com/frochu/html-meta-viewport-setting-69fbb06ed3d8)
* viewport 主要是為了智慧型手機, 平板顯示網頁時畫面較小的問題，那麼為了解決這問題 W3C 在 HTML 制定了專門的標籤語法給網頁使用
* Mobile Device上的 CSS pixel 與 DIPs 是同樣算法: hardware_pixel / pixel_ratio
* 常見的設定
```
<meta name="viewport" content="width=device-width, initial-scale=1" >
// 以下兩種設定都可以防止使用者做畫面縮放，將畫面鎖在縮放比例 100%
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" >
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
```
* width=device-width 瀏覽器顯示寬度 = 裝置寬度
* maximum-scale 最大能放大幾倍


#### CSS 3 Media Query 觀念
* @media(條件){觸發內容}
* 是看當下的螢幕解析度(手機橫放時跟手機直放時, 寬是不同的)
* 響應式網頁設計, 永遠是從最高解析度螢幕做到最低高(max-width), 或是相反(min-width)
* 從解析度大的寫起範例, max-width
```
螢幕解析度寬小於 768px, title 文字變藍色,螢幕解析度寬小於 375px, title 文字變黃色

最大解析度時, 通常是 pc
.title{
    color:red
}

更小一點
@media(max-width:768px){
    .title{
        color:blue;
    }
}

再更小一點, 上面條件也會被打開, 但權重相同時, 後寫的會覆蓋前面的, 因此黃色覆蓋掉藍色
@media(max-width:375px){
    .title{
        color:yellow;
    }
}
```
* 從解析度小的寫起範例 ,min-width
```
最小
.title{
    color:yellow
}

更大一點
@media(min-width:375px){
    .title{
        color:blue;
    }
}

再更大一點
@media(min-width:768px){
    .title{
        color:red;
    }
}
```



## 常見版型佈局設定
* 禁止顯示 x 軸法則, 只能上下滑
* 流體式佈局主要是採用百分比來設置的意思，當瀏覽器畫面放大或縮小時，畫面就會跟著調整，因此流體式佈局又稱之為「響應式設計」
* max-width
```
最大寬度 600px, 解析度寬小於 600px, 自適應伸縮, 一般的 width 不會自適應伸縮, 會出現 x 軸
.wrap{
    max-width: 600px;
    margin: 0 auto;
    background: gray;
}
```
* 中文內文單行字元 30～40 會比較好閱讀，英文則是 32～80 個字元數, 更詳細一點的規範與說明，可以看以下[參考連結](https://www.ibm.com/design/language/typography/type-basics/)
* 一般的 width 會搭配版型單位 % 數觀念, 會看父元素的寬度, 來決定實際 px 是多少, 等於會自適應父元素寬度
* 三欄流體式設計, 注意全部欄位加起來不要超過 100% -project/block2
* 左側 float 固定(寫死寬度)、右側流體式設計 -project/block3
* flex 左側固定(寫死寬度)、右側流體式設計 -project/block4

## 以 UI 設計角度切入響應式設計該注意的細節
* 有些內容可以不用顯示在手機網頁, 會太雜亂, 少即是多, 避免資訊量爆炸
* 斷點時機：設計多欄式佈局的必要觀念
* 中文內文單行字元 30～40 會比較好閱讀，英文則是 32～80 個字元數
* 點擊範圍：設計讓人好點選的元素, 點選時手指範圍高 44 px
* hover 是 pc 滑鼠特有的, 平板手機運用 touch 事件
  * touchstart -> touchmove -> touchend -> click
  * [參考](https://ithelp.ithome.com.tw/articles/10268781?sc=rss.iron) 
* 斷點元素：只有手機才會顯示的功能與Layout切換

## 斷點規劃
* 先要有個認知，響應式無法讓所有螢幕解析度都最佳化
  * 遵循 80/20 法則，先兼容熱門瀏覽器吧！ 
  * 更進一步可以套用 google 分析 [GA](https://israynotarray.com/analytics/20190506/1879363764/)來替網站作分析
* 先決定網頁是手機客群還是 pc 客群, 決定Mobile First 與 Desktop 優先的網頁版型 
* 常見 pc 中間 content 解析度 960, 1000, 1024, 1280, 每次都滿版也不好, 解析度高代表高清圖片, 下載流量大
* 查訊常用手機平板解析度
  * [Viewport Sizes](http://viewportsizes.mattstow.com/)
  * [screensiz.es](https://screensiz.es/)
  * [yesviz](https://yesviz.com/)
* 767 px 以下時就會直接把他變成手機版型, 320px 是你最小需要注意的 SIZE，現在仍然有許多手機是 320px，尤其是小 size 的 iPhone，以前甚至有 240px 的手機，但現在已經相當稀有了。
  * iPad - 768px
  * iPad以下 - 767px
  * iPhone 6 Plus - 414px (視專案族群)
  * iPhone 6 - 375px  (視專案族群)
  * iPhone 5、SE - 320px
  * 再接著 767px~320px 便會視為他們都是手機 size，所以在 767 px 以下時就會直接把他變成手機版型

## 響應式表格與表單設計
### 響應式表格設計 
* 手機不用顯示所有欄位, 用 display none, project/block5
* 或是用 overflow-x: auto; 設定滾動軸單獨出現於表格, project/block6

### pure.css：加強你對網頁元素的了解
* [連結](https://purecss.io/)
* 針對各種瀏覽器做優化
* 模組化
* 不要動別人的模組, 自己加 class 名稱, 用權重方式覆蓋部分樣式
* project/block7

## 響應式圖形設計
### 基礎篇：響應式圖片設計
* 圖片會依照父元素放大且縮小, 圖片放大時會有模糊問題, width: 100%;
* 圖片會依照父元素縮小, 最大就是原圖大小, max-width:100%;

### 響應式圖片 reset 教學
```
css
img{
    max-width:100%;
    height: auto;
}
```

### 心法篇：圖片 SIZE 規劃，刻意設計較大張一點的技巧
* 在斷點切換時, 可能圖片會比預期的大一些, 要測試

### SVG 篇：向量圖片介紹
* 可用於網頁的向量格式，可使用[css3的混合模式](https://www.minwt.com/webdesign-dev/css/20664.html)或是 JS 加上效果，在螢幕上縮放 SVG 影像，而不會失去細部或清晰度
* [線上轉換工具](https://www.vectorizer.io/)
* [線上壓縮圖片工具](https://tinypng.com/)
* [mac 離線壓縮工具](https://imageoptim.com/mac)
* 不推薦的使用方式, 把svg裡面的code整個貼到網頁上(缺點：code會很亂；優點：可以達到我想要的改顏色)
* 用於 logo 搭配圖片取代文字技巧, a 標籤背景圖設定為 svg
  * 設為背景時, 在no-repeat情況下，如果容器寬高比與圖片寬高比不同, background-size： contain 與cover的區別
    * cover：圖片寬高比不變、鋪滿整個容器的寬高，而圖片多出的部分則會被截掉；
    * contain:圖片自身的寬高比不變，縮放至圖片自身能完全顯示出來，所以容器會有留白區域
  * 圖片取代文字 
    ```
    text-indent:101% ; 
    overflow: hidden ; 當元素超出範圍時，元素就隱藏
    white-space: nowrap; 因為文字到達元素最大範圍時，通常會自動換行，但是這邊設置強制不讓他換行。
    ``` 

## 常見響應式設計選單
* 通常網頁文字 16px, iphone 320px = 20字
* 隱藏選單顯示時最好不要推擠原本主要內容, 用覆蓋上去的方式
* 常用技巧 max-height, max-width, overflow, position, transition, z-index, transform

### 多欄多列式
* project/block8

### 漢堡選單設計
* 使用 jQuery project/block9
* 使用 css project/block10, 推薦

### 固定式選單 (position: fixed)
* 注意選單下有沒有遮住內容
* 不要佔太大空間, 因為不管怎麼滑動都會在

### Off Canvas 選單設計
* 使用 jQuery project/block11

## 開始實作前的注意事項
### media 到底要全部寫在一起還是隔離
* 針對大項(如 .menu 或 .header 等)分別去管理

### 設計一套讓客戶能夠自行上稿的 template
* 客戶自行上稿的地方, css 不要用  class 自定名稱, 直接用標籤名稱

### 注意網頁載入速度與效能問題
* 先載入 css(網頁先渲染出現畫面), 再載入 js
* chrom 檢查 網路 可以測試, 3 秒內要完成
* 不要放太多自動播放的動畫效果(效能開銷大), 一個頁面最多兩個

## Sass, Scss
### 減少重工負擔
* Sass, 用空白或 tab
* Scss, 用 {}, 推薦
* 有支援 加減乘除運算
### 變數篇 - 整合相同設定好幫手
* 錢字號加名稱
* 常用於顏色 字體 文字大小等
```
scss

$primary: #30c39e;
.btn {
    background: $primary;
}
```
### 拆檔篇 - @import: 每隻 Sass 檔專心做一件事情
* 模組化, 最後再合併
* CSS @import 與 SCSS @import 兩種做法的用途都是便於拆分、模組化 CSS ，只是兩者有一些差別 :
  * SCSS @import 的作法是將所有要合併的 .scss 檔 ( 如 _reset.scss ) 引入到一支要編譯的 .scss 檔 ( 如 all.scss ) ， 透過編譯器將所有 SCSS 合併到同一支 all.css 。當瀏覽器載入只需引入一支 all.css 。
  * 使用 CSS @import 也可以引入 css 檔案，不過需要注意每一支引入的檔案在瀏覽器執行時皆會發出一次 http 請求，並且會影響到其他檔案的載入順序與速度，容易造成頁面渲染速度較慢的問題。

### 工具篇 - @mixin: 將常用語法化簡為自己的知識庫
* [extend、mixin、function](https://icguanyu.github.io/scss/scss_2/)
* [SASS，SCSS其中@extend與@mixin的差異性](https://medium.com/lion-f2e/%E8%A8%8E%E8%AB%96-sass-scss%E5%85%B6%E4%B8%AD-extend-%E8%88%87-mixin%E7%9A%84%E5%B7%AE%E7%95%B0%E6%80%A7-764d3a38dfbd)
  * 參數：需要傳遞參數的場合， Mixin是最佳的選擇，可便於建立一個自訂義的樣式庫
  * 封裝：兩者在編譯成CSS時有所差異， Mixin時分開拓展，@extend是合併拓展
  * 命名： Mixin是非語義類的可混入特殊符號，@ extend則必須避免特殊符號上的使用。但是extend可無須重新命名，在使用上可避免重複命名的事情發生。
  * 事實上大多數Web Server在加載時會將文本相同的重複塊進行壓縮處理，這意味著，儘管mixin可能產生的CSS多於擴展，但它們可能不會大幅增加用戶下載量。因此，選擇對您的用例最有意義的功能，而不是產生最少CSS的功能。

### 結構篇 - 如何循序漸進優化網頁架構
* 建議 all.scss 結構
```
@import "variable" //所有全域變數
@import "reset" // reset css
@import "mixin"
@import "extend"
@import "grid" // 設定斷點
@import "layout" // 通用版型, 表頭, 表尾
@import "module/button"
@import "module/form"
@import "module/table"
@import "pages/index" // 首頁
@import "pages/cart" // 內頁
```

### 載具篇 - @mixin+@content 設計響應式設計超方便
* 針對各個斷點設定 mixin
* content 代表會把 mixin 外的程式碼加入到 content
```
@mixin pad {
    @media(max-width: 768px){
      @content  
    }
}

p{
    width: 100%;
    @include pad(){
        width: auto;
    }
}
```