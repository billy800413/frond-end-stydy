function showSlide(slides, slideIndex, displayVal){
    var i;
    for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
    }
    slides[slideIndex-1].style.display = displayVal;
}
export function clickRight(slides, slideIndex, displayVal){
    var numSlide = slides.length;
    var slideIndexNew;
    if (slideIndex == numSlide){ 
    slideIndexNew = 1;
    }
    else{
    slideIndexNew = slideIndex + 1;
    }
    showSlide(slides, slideIndexNew, displayVal);
    return slideIndexNew;
}
export function clickLeft(slides, slideIndex, displayVal){
    var numSlide = slides.length;
    var slideIndexNew;
    if (slideIndex == 1){ 
    slideIndexNew = numSlide;
    }
    else{
    slideIndexNew = slideIndex - 1;
    }
    showSlide(slides, slideIndexNew, displayVal);
    return slideIndexNew;
}
export function backToNormal(slides, displayVal){
    var i;
    for (i = 0; i < slides.length; i++){
        slides[i].style.display = displayVal;
    }
    var mobileMode = false;
    var slideIndex = -1;
    return [mobileMode, slideIndex]
}
export function init(openMobileModeWidth, slides, displayVal){
    var windowsize = $(window).width();
    var mobileMode = false;
    var slideIndex = -1;
    if (windowsize <= openMobileModeWidth){
    mobileMode = true;
    slideIndex = 1;
    showSlide(slides, slideIndex, displayVal);
    }
    return [mobileMode, slideIndex];
}