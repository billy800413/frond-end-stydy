import { init, clickRight, clickLeft, backToNormal } from './mySlide.js';

$(document).ready(function () {
    let featureSlides = document.getElementsByClassName("feature-slide");
    let adSlides = document.getElementsByClassName("AD-slide");
    let openMobileModeWidth = 567;
    let [mobileMode, featureSlideIndex] = init(openMobileModeWidth, featureSlides, "flex");
    let adSlideIndex = -1;
    [mobileMode, adSlideIndex] = init(openMobileModeWidth, adSlides, "flex");

    $(window).resize(function() {
      var windowsize = $(window).width();
      if (windowsize <= openMobileModeWidth && mobileMode == false){
        [mobileMode, featureSlideIndex] = init(openMobileModeWidth, featureSlides, "flex");
        [mobileMode, adSlideIndex] = init(openMobileModeWidth, adSlides, "flex");
      }
      else if (windowsize > openMobileModeWidth && mobileMode == true){
        [mobileMode, featureSlideIndex] = backToNormal(featureSlides, "flex");
        [mobileMode, adSlideIndex] = backToNormal(adSlides, "flex");
      }
    });


    $(".feature .right").click(function (e) { 
      e.preventDefault();
      featureSlideIndex = clickRight(featureSlides, featureSlideIndex, "flex"); 
    });
    $(".feature .left").click(function (e) { 
      e.preventDefault();
      featureSlideIndex = clickLeft(featureSlides, featureSlideIndex, "flex");
    });

    $(".AD .right").click(function (e) { 
      e.preventDefault();
      adSlideIndex = clickRight(adSlides, adSlideIndex, "flex"); 
    });
    $(".AD .left").click(function (e) { 
      e.preventDefault();
      adSlideIndex = clickLeft(adSlides, adSlideIndex, "flex");
    });

});