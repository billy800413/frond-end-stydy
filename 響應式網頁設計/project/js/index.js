$(document).ready(function () {
    
    $(".block9 .show-menu").click(function (e) { 
        e.preventDefault();
        $(this).parent().find(".menu").slideToggle(1000);
    });
    $(window).resize(function() {
        var windowsize = $(window).width();
        if (windowsize > 767) {
            $(".block9 .menu").css("display", "flex");
            $(".block9 .menu").css("position", "static");
            $(".block9 .menu").css("flex-direction", "row");
        }
        else{
            $(".block9 .menu").css("display", "none");
            $(".block9 .menu").css("position", "absolute");
            $(".block9 .menu").css("flex-direction", "column");
        }
    });

    $(".block10 .show-menu").click(function (e) { 
        e.preventDefault();
        $('.block10 .menu').toggleClass('active');
    });

    $(".block11 .mobile-link").click(function (e) { 
        e.preventDefault();
        $('.block11 .aside').toggleClass('active');
    });
    $(".block11 .mobile-close").click(function (e) { 
        e.preventDefault();
        $('.block11 .aside').removeClass('active');
    });

    
});