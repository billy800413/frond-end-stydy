// 使用 node.js 的模組 path
const path = require('path');

module.exports = {
  entry: './src/index.js',
  output: {
    // __dirname 當前路徑
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
  },
};