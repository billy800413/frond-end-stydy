import axios from "axios";
const apiUrl = "https://todoo.5xcamp.us";
axios.defaults.baseURL = apiUrl;


function signUp(email, nickname, pwd, callback){
  axios.defaults.headers.common['Authorization'] = "";
  return axios.post(`/users`, {
        "user": {
            "email": email,
            "nickname": nickname,
            "password": pwd
        }
      })
      .then(function (response) {
        console.log("signUp success, 自動跳轉到 SignIn");
        callback(email, pwd);
      })
      .catch(function (error) {
        console.log("signUp error");
        console.log(error.response);
        alert("註冊失敗, 請換個帳號試試");
      });
}

function signIn(email, pwd, callback){
  return axios.post(`/users/sign_in`, {
        "user": {
            "email": email,
            "password": pwd
        }
      })
      .then(function (response) {
        axios.defaults.headers.common['Authorization'] = response.headers.authorization;
        callback(response.data.nickname);
      })
      .catch(function (error) {
        console.log("signIn error");
        console.log(error.response);
        if(error.response.data.message=="登入失敗"){
          alert("請先註冊帳號");
        }
        else{
          alert("發生錯誤, 請稍後再試");
        }
      });
}

function logOut(callback){
  return axios.delete(`/users/sign_out`)
      .then(function (response) {
        axios.defaults.headers.common['Authorization'] = "";
        callback();
      })
      .catch(function (error) {
        console.log("logout error");
        console.log(error.response);
        alert("發生錯誤, 請稍後再試");
      });
}

function getTodo(getTodoCallback){
  return axios.get(`/todos`)
      .then(function (response) {
        getTodoCallback(response.data.todos);
      })
      .catch(function (error) {
        console.log("getTodo error");
        console.log(error.response);
        alert("發生錯誤, 請稍後再試");
      });
}

function addTodo(newTodo, callback){
  return axios.post(`/todos`, 
        {
          "todo": {
                "content": newTodo
          }
        }
      )
      .then(function (response) {
        callback(response.data);
      })
      .catch(function (error) {
        console.log("addTodo error");
        console.log(error.response);
        alert("發生錯誤, 請稍後再試");
      });
}

function modifyTodo(todoItem, index, newContent, modifyTodoCallback){
  return axios.put(`/todos/${todoItem.id}`, 
        {
          "todo": {
                "content": newContent
          }
        }
      )
      .then(function (response) {
        modifyTodoCallback(index, newContent);
      })
      .catch(function (error) {
        console.log("modifyTodo error");
        console.log(error.response);
        alert("發生錯誤, 請稍後再試");
      });
}

function toggleTodo(todoItem, index, toggleTodoCallback){
  return axios.patch(`/todos/${todoItem.id}/toggle`)
      .then(function (response) {
        toggleTodoCallback(index, response.data.completed_at);
      })
      .catch(function (error) {
        console.log("toggleTodo error");
        console.log(error.response);
        alert("發生錯誤, 請稍後再試");
      });
}

function delTodo(todoItem, index, delTodoCallBack){
  return axios.delete(`/todos/${todoItem.id}`)
      .then(function (response) {
        delTodoCallBack(index);
      })
      .catch(function (error) {
        console.log("delTodo error");
        console.log(error.response);
        alert("發生錯誤, 請稍後再試");
      });
}

function delTodoAll(toDoList, filter, delTodoCallBack){
  let index;
  let promiseList = [];
  for (index = 0; index < toDoList.length; index++) {
    if(toDoList[index] == null){
      continue; 
    }
    if (filter == "全部"){
      promiseList.push( delTodo(toDoList[index], index, delTodoCallBack) );
    }
    else if(filter == "已完成" && toDoList[index].completed_at != null ){
      promiseList.push( delTodo(toDoList[index], index, delTodoCallBack) );
    }
    else if(filter == "待完成" && toDoList[index].completed_at == null ){
      promiseList.push( delTodo(toDoList[index], index, delTodoCallBack) );
    }
  }
  return Promise.all(promiseList)
      .then(responses => {})
      .catch(errors => console.log(errors));
}

async function delTodoAllAndGetTodo(toDoList, filter, delTodoCallBack, getTodoCallback){
  try {
    await delTodoAll(toDoList, filter, delTodoCallBack);
    await getTodo(getTodoCallback);
  } 
  catch (error) {
    console.error(error);
  }
}


export default {"signUp": signUp,
                "signIn": signIn,
                "logOut": logOut,
                "getTodo": getTodo,
                "addTodo": addTodo,
                "modifyTodo": modifyTodo,
                "toggleTodo": toggleTodo,
                "delTodo": delTodo,
                "delTodoAllAndGetTodo": delTodoAllAndGetTodo};