import api from "./function.js";

function initGlobalParameters(){
    toDoList = [];
    numTodo = 0;
    numDone = 0;
}

function signUpCallback(email, pwd){
    console.log("signUpCallback");
    api.signIn(email, pwd, signInCallback);
}

function signInCallback(nickname){
    console.log("signInCallback");
    mainHeaderP.textContent = `${nickname}的待辦清單`;
    initGlobalParameters();
    changePage("main");
    api.getTodo(getTodoCallback);
}

function logOutCallback(){
    console.log("logOutCallback");
    initGlobalParameters();
    changePage("signIn");
}

function toggleTodoCallback(index, newValue){
    toDoList[index].completed_at = newValue;
    if (newValue == null){
        numTodo += 1;
        numDone -= 1;
    }
    else{
        numTodo -= 1;
        numDone += 1;
    }
    updateUl();
    console.log("toggleTodoCallback")
    console.log(toDoList[index]);
}

function modifyTodoCallback(index, newContent){
    toDoList[index].content = newContent;
    updateUl();
    console.log("modifyTodoCallback")
    console.log(toDoList[index]);
}

function addTodoCallback(data){
    let newTodo = {"id": data.id,
                    "content": data.content,
                    "completed_at": null};
    toDoList.push(newTodo);
    numTodo += 1;
    updateUl();
    console.log("addTodoCallback");
    console.log(toDoList);
}

function delTodoCallBack(index, notChangeLength=true){
    if(toDoList[index].completed_at == null){
        numTodo -= 1;
    }
    else{
        numDone -= 1;
    }
    if (notChangeLength){
        toDoList.splice(index, 1, null);
    }
    else{
        toDoList.splice(index, 1);
    }
    
    updateUl();
    console.log("delTodoCallBack");
    console.log(toDoList);
    console.log(`numTodo ${numTodo}`);
    console.log(`numDone ${numDone}`);
}

function getTodoCallback(severDataList){
    initGlobalParameters();
    severDataList.forEach(function(item, index){
        toDoList.push(item);
        if (item.completed_at == null){
            numTodo += 1;
        }
        else{
            numDone += 1;
        }
    })
    updateUl();
    console.log("getTodoCallback");
    console.log(toDoList);
}

function createLi(item, index){
    let text = "";
    let checked = "";
    let contentDone = "";
    if(item == null){
        return text;
    }
    const content = item.content;
    const alreadyDone = item.completed_at != null;
    if(filterState == "待完成" && alreadyDone){
        return text;
    }
    else if(filterState == "已完成" && alreadyDone == false){
        return text;
    }
    if(alreadyDone){
        checked = "checked";
        contentDone = "done";
    }
    text = `<li data-index="${index}" class="todo_item">
    <input class="toggleDone" type="checkbox" name="toggleDone" id="toggleDone" ${checked}>
    

    <div class="content-wrap">
        <div class="content-show-wrap index-${index}">
            <h2 class="content ${contentDone}">${content}</h2>
            <a class="show-modify-btn" href="#"><i class="fa-solid fa-pen-to-square"></i></a>
            <a class="delete-btn" href=""><i class="fa-solid fa-trash"></i></a>
        </div>
        <div class="modify-wrap hide index-${index}"">
            <input class="content" type="text" name="modifyContent" id="modifyContent-${index}" placeholder="請輸入修改內容">
            <a class="modify-confirm-btn" href="#"><i class="fa-solid fa-check"></i></a>
            <a class="modify-cancel-btn" href="#"><i class="fa-solid fa-rotate-left"></i></a>
        </div>
    </div>

    
    
    </li>`;
    return text;
}

function updateUl(){
    if (numDone + numTodo == 0){
        showDiv.classList.add("hide");
        emptyDiv.classList.remove("hide");
        return;
    }
    else{
        showDiv.classList.remove("hide");
        emptyDiv.classList.add("hide");
    }
    let text = "";
    toDoList.forEach(function(item, index){
        text += createLi(item, index);
    })
    todoListArea.innerHTML = text;

    if(filterState == "全部"){
        summaryP.textContent = `${numTodo}個待完成事項, ${numDone}個已完成事項`;
        delAllBtn.textContent = `清除全部`;
    }
    else if(filterState == "待完成"){
        summaryP.textContent = `${numTodo}個待完成事項`;
        delAllBtn.textContent = `清除全部待完成`;
    }
    else{
        summaryP.textContent = `${numDone}個已完成事項`;
        delAllBtn.textContent = `清除全部已完成`;
    }
}

function changePage(pageNow){
    const loginPage = document.querySelector(".login");
    const signUpPage = document.querySelector(".signUp");
    const mainPage = document.querySelector(".main");
    if(pageNow == "main"){
        signUpPage.classList.add('hide');
        mainPage.classList.remove('hide');
        loginPage.classList.add('hide');
    }
    else if(pageNow == "signUp"){
        signUpPage.classList.remove('hide');
        mainPage.classList.add('hide');
        loginPage.classList.add('hide');
    }
    else{
        signUpPage.classList.add('hide');
        mainPage.classList.add('hide');
        loginPage.classList.remove('hide');
    }
}


// ===================global parameter
let filterState = "全部";
let toDoList = [];
let numTodo = 0;
let numDone = 0;
changePage("login");
initGlobalParameters();
// ===================global parameter end
//====================login
const loginBtn = document.querySelector(".login-btn");
const goSignUpBtn = document.querySelector(".go-signUp-btn");
const loginEmailInput = document.querySelector("#login-email");
const loginPwdInput = document.querySelector("#login-pwd");
loginBtn.addEventListener("click", function(e){
    e.preventDefault();
    const email = loginEmailInput.value;
    const pwd = loginPwdInput.value;
    api.signIn(email, pwd, signInCallback);
    loginEmailInput.value = "";
    loginPwdInput.value = "";
})
goSignUpBtn.addEventListener("click", function(e){
    e.preventDefault();
    changePage("signUp");
})
//====================login end
//====================signUp
const signUpBtn = document.querySelector(".signUp-btn");
const goLoginBtn = document.querySelector(".go-login-btn");
const signUpEmailInput = document.querySelector("#signUp-email");
const signUpNicknameInput = document.querySelector("#signUp-nickname");
const signUpPwdInput = document.querySelector("#signUp-pwd");
const signUpPwdAgainInput = document.querySelector("#signUp-pwd-again");
goLoginBtn.addEventListener("click", function(e){
    e.preventDefault();
    changePage("login");
})
signUpBtn.addEventListener("click", function(e){
    e.preventDefault();
    console.log("click signUp btn"); 
    const email = signUpEmailInput.value;
    const nickname = signUpNicknameInput.value;
    const pwd = signUpPwdInput.value;
    const pwdAgain = signUpPwdAgainInput.value;
    console.log("click signUpBtn");
    console.log(email, nickname, pwd, pwdAgain);
    if(email == ""){
        console.log("email不可為空白");
        alert("email不可為空白");
        return;
    }
    if(nickname == ""){
        console.log("請輸入暱稱");
        alert("請輸入暱稱");
        return;
    }
    if(pwd == ""){
        console.log("密碼不可為空白");
        alert("密碼不可為空白");
        return;
    }
    else if(pwd.length < 6){
        console.log("密碼強度不夠, 請至少6個字");
        alert("密碼強度不夠, 請至少6個字");
        return;
    }
    else if (pwd != pwdAgain){
        console.log("兩次密碼不相同");
        alert("兩次密碼不相同");
        return;
    }
    api.signUp(email, nickname, pwd, signUpCallback);
})
//====================signUp end
//====================main
const mainHeaderP = document.querySelector(".main-header p");
const addNewArea = document.querySelector(".add-new");
const addNewContent = document.querySelector(".add-new .new-content");
const todoListArea = document.querySelector(".todo-list");
const showDiv = document.querySelector(".show");
const emptyDiv = document.querySelector(".main-empty");
const filterAllBtn = document.querySelector(".filter .all");
const filterNotYetBtn = document.querySelector(".filter .not-yet");
const filterDoneBtn = document.querySelector(".filter .done");
const delAllBtn = document.querySelector(".main .summary .del-all-btn");
const summaryP = document.querySelector(".main .summary p");
const logOutBtn = document.querySelector(".main .logout-btn");
delAllBtn.addEventListener("click", function(e){
    e.preventDefault();
    console.log("click delAllBtn")
    api.delTodoAllAndGetTodo(toDoList, filterState, delTodoCallBack, getTodoCallback);
})
logOutBtn.addEventListener("click", function(e){
    e.preventDefault();
    console.log("click logoutBtn")
    api.logOut(logOutCallback);
})
todoListArea.addEventListener("click", function(e){
    if (["I", "INPUT"].includes(e.target.nodeName) == false){
        return;
    }
    console.log("todoListArea", e.target);
    const index = e.target.closest('li').getAttribute("data-index");
    if(e.target.nodeName == "I"){
        e.preventDefault();
        const btn = e.target.closest('a').className;
        const contentShowWrap = document.querySelector(`.content-show-wrap.index-${index}`);
        const modifyWrap = document.querySelector(`.modify-wrap.index-${index}`);
        console.log("click", btn, btn == "show-modify-btn");
        if(btn == "delete-btn"){
            api.delTodo(toDoList[index], index, delTodoCallBack);
        }
        else if(btn == "show-modify-btn"){
            modifyWrap.classList.remove('hide');
            contentShowWrap.classList.add('hide');
        }
        else if(btn == "modify-cancel-btn"){
            modifyWrap.classList.add('hide');
            contentShowWrap.classList.remove('hide');
        }
        else if(btn == "modify-confirm-btn"){
            const modifyContent = document.querySelector(`#modifyContent-${index}`).value;
            if (modifyContent == ""){
                alert("修改內容不能為空白");
                return;
            }
            api.modifyTodo(toDoList[index], index, modifyContent, modifyTodoCallback);
        }
    }
    else if(e.target.getAttribute("type") == "checkbox"){
        let nowState = e.target.checked;
        let previousState = toDoList[index].completed_at != null;
        if (nowState != previousState){
            api.toggleTodo(toDoList[index], index, toggleTodoCallback);
        }
    }
})

addNewArea.addEventListener("click", function(e){
    e.preventDefault();
    if (["A", "I"].includes(e.target.nodeName) == false){
        return;
    }
    const newContent = addNewContent.value;
    if(newContent == ""){
        alert("待辦事項不可為空白");
        return;
    }
    api.addTodo(newContent, addTodoCallback);
    addNewContent.value = "";
})

filterAllBtn.addEventListener("click", function(e){
    e.preventDefault();
    filterAllBtn.classList.add('active');
    filterNotYetBtn.classList.remove('active');
    filterDoneBtn.classList.remove('active');
    filterState = "全部";
    updateUl();
})
filterNotYetBtn.addEventListener("click", function(e){
    e.preventDefault();
    filterAllBtn.classList.remove('active');
    filterNotYetBtn.classList.add('active');
    filterDoneBtn.classList.remove('active');
    filterState = "待完成";
    updateUl();
})
filterDoneBtn.addEventListener("click", function(e){
    e.preventDefault();
    filterAllBtn.classList.remove('active');
    filterNotYetBtn.classList.remove('active');
    filterDoneBtn.classList.add('active');
    filterState = "已完成";
    updateUl();
})
