let a = 1;
let b = "234";
let c = null;
console.log(a, b);
console.log(a, b, c);

let el = document.querySelector("h2");
console.log(el);
el.textContent += "3333";

const main = document.querySelector(".main");
console.log(main);
let txt = "測試456"
main.innerHTML = `<p>${txt}</p>`;
console.log(main);

let myLink = "https://tw.yahoo.com/"
const list = document.querySelector(".list");
console.log(typeof list);
let liText = `<li><a href="${myLink}">連結</a></li>`
list.innerHTML = liText + liText

aLink = document.querySelector(".list li a");
console.log(aLink.nodeName);
aLink.setAttribute("href", "https://www.google.com/");
aLink.setAttribute("class", "newClassName");

let block2BtnClickCount = 0;
const block2Btn = document.querySelector(".block2 .btn");
const block2H2 = document.querySelector(".block2 h2");
block2Btn.addEventListener("click", function(e){
    block2BtnClickCount += 1;
    block2H2.textContent = `${block2BtnClickCount}`
    console.log(e.target)
})

let data = [1,2,3, [4,6]];
data.forEach(function(item, index, array){
    console.log(item, index, array)
})

//=========================

let personList = [];
let numMale = 0;
let numFeMale = 0;
const block3AddBtn = document.querySelector(".block3 .upload-btn");
const inputName = document.querySelector(".block3 .input-name");
const inputSex = document.querySelector(".block3 .input-sex");
const sexNow = document.querySelector(".block3 .sex-now");
const table = document.querySelector(".block3 table");
const showFormat = document.querySelector(".block3 .show-format");
let showState = "全部";
const tableHeader = `<tr><th>姓名</th><th>姓別</th><th>刪除</th></tr>`

function createTR(name, sex, index){
    let trText = `<tr><td>${name}</td><td>${sex}</td><td><input type="button" value="刪除" class="del-${index}-btn"></td></tr>`
    return trText
}
function createTable(personList){
    let tableSummary = tableHeader;
    personList.forEach(function(item, index){
        let trText = createTR(item.name, item.sex, index);
        if (showState == "全部"){
            tableSummary += trText;
        }
        else if(showState == item.sex){
            tableSummary += trText;
        }
    })
    return tableSummary;
}
console.log(inputName);
console.log(block3AddBtn);
block3AddBtn.addEventListener("click", function(e){
    let person = {
        name: inputName.value,
        sex: inputSex.value
    };
    if(person.sex == "男性"){
        numMale += 1;
    }
    else{
        numFeMale +=1;
    }
    personList.push(person);
    sexNow.textContent = `男${numMale}人, 女${numFeMale}人`
    table.innerHTML = createTable(personList);
})
table.addEventListener("click", function(e){
    if(e.target.nodeName=="INPUT"){
        let index = e.target.className.split("-")[1];
        index = parseInt(index);
        if(personList[index].sex == "男性"){
            numMale -= 1;
        }
        else{
            numFeMale -=1;
        }
        personList.splice(index, 1);
        sexNow.textContent = `男${numMale}人, 女${numFeMale}人`
        table.innerHTML = createTable(personList);
    }
})
showFormat.addEventListener("click", function(e){
    if(e.target.nodeName=="INPUT"){
        showState = e.target.value;
        table.innerHTML = createTable(personList);
    }
})

//===========================
const block4p = document.querySelector(".block4 p");
const block4PostSignupBtn = document.querySelector(".block4 .post-signup-btn");
const block4PostSigninBtn = document.querySelector(".block4 .post-signin-btn");
const block4Email = document.querySelector(".block4 .block4Email");
const block4Password = document.querySelector(".block4 .block4Password");
const block4PostMessage = document.querySelector(".block4 .post-message");
function renderData(response){
    block4p.textContent = `這是get ${response.data[0].name}`
}
axios.get("https://hexschool.github.io/ajaxHomework/data.json").then(function (response) {
    console.log(response);
    renderData(response);
  })
  .catch(function (error) {
    console.log(error);
  })
  .finally(function () {
    // always executed
  });
block4PostSignupBtn.addEventListener("click", function(e){
    let postData = {
        email: `${block4Email.value}`,
        password: `${block4Password.value}`
    }
    console.log("postData", postData)
    axios.post('https://hexschool-tutorial.herokuapp.com/api/signup', postData)
      .then(function (response) {
        console.log(response);
        block4PostMessage.textContent = response.data.message;
      })
      .catch(function (error) {
        console.log(error);
      });
})
block4PostSigninBtn.addEventListener("click", function(e){
    let postData = {
        email: `${block4Email.value}`,
        password: `${block4Password.value}`
    }
    console.log("postData", postData)
    axios.post('https://hexschool-tutorial.herokuapp.com/api/signin', postData)
      .then(function (response) {
        console.log(response);
        block4PostMessage.textContent = response.data.message;
      })
      .catch(function (error) {
        console.log(error);
      });
})
//======================================
let block5TodoList = [];
let block5StateFilterNow = "全部";
const block5AddBtn = document.querySelector(".block5 .add-btn");
const block5NewTitle = document.querySelector(".block5 .new-title");
const block5NewState = document.querySelector(".block5 .new-state");
const block5NewContent = document.querySelector(".block5 .new-content");
const block5ShowToDoList = document.querySelector(".block5 .todo-list");
const block5StateFilterOpt = document.querySelector(".block5 .state-filter-opt");
const block5FilterBtn= document.querySelector(".block5 .filter-btn");

function block5CreateLi(title, content, view, index){
    let activeView = "";
    if(view){
        activeView = "view";
    }
    let text = `<div class="head">
    <h2>${title}</h2>
    <div class="edit">
        <input type="button" class="view" value="檢視" data-index="${index}">
        <input type="button" class="del" value="刪除" data-index="${index}">
    </div>
    </div>
    <p class="content data-index-${index} ${activeView}">${content}</p>`
    return text
}
function block5UpDateUl(todoList){
    let text = "";
    todoList.forEach(function(item, index){
        if (block5StateFilterNow == "全部" || block5StateFilterNow == item.state){
            text += block5CreateLi(item.title, item.content, item.view, index);
        }
    })
    block5ShowToDoList.innerHTML = text;
}
block5ShowToDoList.addEventListener("click", function(e){
    if(e.target.nodeName != "INPUT"){
        return;
    }
    let dataIndex = e.target.getAttribute("data-index");
    dataIndex = parseInt(dataIndex);
    let className = e.target.className;
    if (className == "del"){
        block5TodoList.splice(dataIndex, 1);
        block5UpDateUl(block5TodoList);
        return;
    }
    else if(className == "view"){
        let block5ToDoListContent = document.querySelector(`.block5 .todo-list .content.data-index-${dataIndex}`);
        block5ToDoListContent.classList.toggle("view");
        if(block5ToDoListContent.classList.contains("view")){
            block5TodoList[dataIndex].view = true;
        }
        else{
            block5TodoList[dataIndex].view = false;
        }
        return ;
    }
})
block5AddBtn.addEventListener("click", function(e){
    let title = block5NewTitle.value;
    let content = block5NewContent.value;
    let state = block5NewState.value;
    if(title.length == 0){
        return;
    }
    if(content.length == 0){
        content = title;
    }
    let todo = {
        "title": title,
        "content": content,
        "state": state,
        "view": false,
    }
    block5TodoList.push(todo);
    block5UpDateUl(block5TodoList);
})
block5FilterBtn.addEventListener("click", function(e){
    block5StateFilterNow = block5StateFilterOpt.value;
    block5UpDateUl(block5TodoList);
})

//================
let arry7 = [1, 2, 3]
console.log(arry7);
let newArryMap7 = arry7.map(function(item){
    return item * 5;
})
let newArryFilter7 = arry7.filter(function(item){
    // 滿足條件的 item 才會放到新陣列
    return item >= 2;
})
let newFind7 = arry7.find(function(item){
    return item >= 5;
})
let newFindIndex7 = arry7.findIndex(function(item){
    return item >= 5;
})
console.log(newArryMap7);
console.log(newArryFilter7);
console.log(newFind7);
console.log(newFindIndex7);
//=========
const numC = (x) =>{
    return x * x;
}
const numD = (x) => x * x;
const numE = x => x * x;
const numF = () => 123;
console.log(numC(5));
console.log(numD(5));
console.log(numE(5));
console.log(numF());
let arry8 = [1, 2, 3]
console.log(arry8);
let newArryMap8 = arry8.map(item =>item * 4)
console.log(newArryMap8);



fetch('https://raw.githubusercontent.com/hexschool/2021-ui-frontend-job/master/frontend_data.json')
    .then(response => response.json())
    .then(data => console.log(data))
    .catch(error => console.log(error));

