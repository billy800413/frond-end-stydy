const checkScore = (score, msec) =>{
    return new Promise((resolve, reject)=>{
        console.log("正在觀察是否及格");
        setTimeout(() => {
            if (score>=60){
                resolve(score);
            }
            else{
                reject("不及格");
            }
        }, msec);
    })
}


checkScore(10, 1000)
    .then(data=>console.log(data))
    .catch(error=>console.log(error));

checkScore(100, 100)
    .then(data=>console.log(data))
    .catch(error=>console.log(error));

/*
async function getData() {
    try { // 專注在成功
        const data1 = await checkScore(60, 100);
        console.log(data1);
        const data2 = await checkScore(10, 100);
        console.log(data2);
    }
    catch (err) { // 專注在錯誤
        console.log('catch', err);
    }
}
getData();


async function getDataAll() {
    try { // 專注在成功
        const data = await Promise.all([checkScore(65, 500), checkScore(100, 1000)]);
        console.log(data);
    }
    catch (err) {
        console.log('catch', err);
    }
}
getDataAll();
*/