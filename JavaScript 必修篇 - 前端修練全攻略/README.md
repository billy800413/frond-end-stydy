# 六角學園的課程筆記

## JavaScript 必修篇 - 前端修練全攻略

## 變數與資料型別
* JavaScript 是動態型別
* 動態語言(Dynamically Typed Languages)：
 * 型別檢查(Type Checking)發生在執行時期(Runtime)。
 * 程式撰寫時不用明確的型別宣告。
 * 執行時，變數能任意更換型別。

### 如何知道變數型別
* typeof 變數名稱;
### 變數名稱方式
* 小駝峰命名, 字首小寫, 第二字首大寫, 中間沒空白
* 變數字首不能為數字

## let、const、var 介紹
* 作用域不一樣，var 的作用域在函數 (function) 裡，let/const 的作用域則是在區塊 (block) 裡。區塊作用域則是比函數作用域較小、較為精確。

```
let/const: { block scope }

var: function(){

function scope

}

Global →Function → Block
```
### let, const
* let 變數名稱 = value;
* const 變數名稱 = value;
### var
* 通常現在不建議使用

## 網頁與 Code 環境建立流程教學
* 用 html 載入 js, 再用  chrom 開啟 html
* 善用 console.log(變數名稱); debug

## 數字型別與賦值運算子
* 先運算，運算結束後再加減
* 異常時會 NaN
```
let x = 5;
let y = x++; // 取 x 值賦予到 y 變數，此行執行完畢後再將 x + 1
console.log(x); // 6
console.log(y); // 5
```
* 先加減並做運算
```
let x = 5;
let y = ++x; // 先將 x + 1 後, 再將值賦予到 y 變數
console.log(x); // 6
console.log(y); // 6
```

## 字串型別
* 可以用單引號或雙引號
* 字串相加
* 數字與字串相加, JavaScript 會自動將數字轉型成字串處理
* parseInt()與 parseFloat() 都是專門將 String 轉換為數字的方法。一個是整數，另一個是浮點數。Number() 則是針對各類型進行數字的轉換。
* 查詢字元數 x.length
* 去除前後空白 x.trim()
* 數字轉型為字串 x.toString()
* 樣板字面值（Template literals）是允許嵌入運算式的字串字面值（string literals), ` ` 字元封閉，代替了雙或單引號。
```
`string text ${expression} string text`
```

## 變數：布林、undefined、null
* undefined, 有宣告但尚未賦予值, undefined 本身也是個型別
* null, 賦予空值, 用來清空資料用, 釋放記憶體, 型別是 object

## 比較與邏輯運算子
* ==、=== 差異講解
  * ===, !== ... 嚴謹模式, 會先檢查型別, 型別不同就直接 false
  * ==, !=... 比較前如需要自動轉型
* 邏輯運算子介紹
  * 且 &&
  * 或 ||

## 流程判斷 - if、else if、else
```
if (條件){
    執行程式碼;
}
else if(條件){
    執行程式碼;
}
else{
    執行程式碼;
}
```

## 陣列教學
* 資料集合
* [JavaScript 陣列處理方法](https://www.hexschool.com/2017/09/01/2017-09-01-javascript-for/)
* [跟 python 比較](https://ischurov.github.io/pythonvjs/)
* 陣列裡可以放任何資料型態, 混合放也沒問題
```
let x = ["a", "b", 1, 2, 3];
```
* 陣列長度 x.length;
* 陣列讀取教學, 陣列 index 從 0 開始
```
x[0];
x[x.length-1];
```
* push 寫入資料在最後 x.push("d");
* 一次 push 兩筆資料 x.push("a", "b"); 相當於 python extend
* 新增資料在第一比 x.unshift("d"); 相當於 python ls.insert(0, "new")
* pop 取出最後一個資料回傳並從陣列刪除
* shift 取出第一個資料回傳並從陣列刪除
* splice(startIndex, numDataToDel) 刪除指定資料
  * startIndex, 刪除的起始點
  * numDataToDel, 包含起始點往後刪除多少筆資料

### 陣列操作
* forEach
  * function 裡不會有 return 
* map
  * 運算後回傳一個新陣列, 不會影響到原本陣列
  * 如果 map function 沒有 return, 會自動變成 return undefined
  ```
  let arry7 = [1, 2, 3]
  let newArryMap7 = arry7.map(function(item){
      return item * 5;
  })
  ``` 
* filter
  * 篩選後回傳一個新陣列, 新陣列內容為符合條件
  ```
  let arry7 = [1, 2, 3]
  let newArryFilter7 = arry7.filter(function(item){
    // 滿足條件的 item 才會放到新陣列
    return item >= 2;
  })
  ```
* find 
  * 回傳尋找到的頭一筆資料, 回傳的是 item
  * 無結果回傳 undefined
  ```
  let arry7 = [1, 2, 3]
  let newFind7 = arry7.find(function(item){
    // 滿足條件的 item, 回傳, 之後就不再尋找
    return item >= 1;
  })
  ``` 
* findIndex
  * 回傳尋找到的頭一筆資料的 index, 回傳的是 int
  * 無結果回傳 -1
  ```
  let arry7 = [1, 2, 3]
  let newFindIndex7 = arry7.findIndex(function(item){
    return item >= 5;
  })
  ```
## 物件教學
* 大括號{}
* 結構像是 python 的 dict, 物件裡的屬性像當於 python dict 的 key, 不同在於 Python dict 的 key 必須是能 hashable (e.g. a string, a number, a float), 但 JavaScript 沒限制
```
let javascriptObject = { name: 'Alexander', year: 1799 };
pythonDict = {'name': 'Alexander', 'year': 1799}
```
* 查找與新增屬性內容
  * 用點
    ```
    javascriptObject.name;
    javascriptObject.newMember = "123";
    ```
  * 用中括號裡面放字串
    ```
    javascriptObject["name"];
    javascriptObject["newMember"] = "123";
    ```

* 如查詢的屬性不存在, 回傳 undefined
* 刪除屬性, 就算原本屬性不存在也不會報錯
```
delete javascriptObject.name;
```

## 函式 function
* 被註冊(宣告)後等待被呼叫
### 函式寫法教學
```
let globalP = 0;
function functionName(p){
    // 先在input 與內部找參數(變數), 沒有往外找
    let a = p + 1;
    globalP += 1
    return p;
}
```

## DOM - 選取網頁元素
* 網頁(html)解析完會自動產生 DOM(Document Object Model) tree

### querySelector 選擇器
* 跟 css 選擇器一樣, 標籤 類別 id選擇器
* 會得到第一個滿足的選擇器(同時有兩個滿足只會有一個)
* document.querySelector(....);
* 得到的是個 DOM 中的節點, type 也是 object

### textContent 得到標籤文字資料
* 只能得到或更改文字
```
const el = document.querySelector("h2");
el.textContent = "3333";
```

### innerHTML 得到 HTML 標籤結構
* 如果是賦予值, 會先清空再加入新的html網頁結構內容
* 注意符號是用 `
* 可以同時定義 html標籤, 文字, 屬性等
```
const main = document.querySelector(".main");
let txt = "測試456"
main.innerHTML = `<p>${txt}</p>`;

let myLink = "https://tw.yahoo.com/"
const list = document.querySelector(".list");
let liText = `<li><a href="${myLink}">連結</a></li>`
list.innerHTML = liText + liText
```

### setAttribute getAttribute 增加得到 HTML 標籤屬性
```
aLink = document.querySelector(".list li a");
aLink.setAttribute("href", "https://www.google.com/");
aLink.setAttribute("class", "newClassName");
```

### querySelectorAll 可重複選取多個元素
* 回傳陣列節點, 相當於陣列
* 跟陣列一樣的操作方式, 用 index 操作

### 一些預設的HTML Attribute, 可以直接用點取賦予值
* .value
* .value 取出來的值都會是字串

### 透過 nodeName 瞭解目前 DOM 的 HTML 標籤
* .nodeName, 回傳大寫 HTML 標籤

## Event 事件 - 讓您的網頁具有互動效果
* addEventListener 註冊事件監聽教學
* 第一個參數為監聽的事件類型
* e 代表點擊當下的一些狀態, 型態為 object, 包含滑鼠座標等
* e.target - 了解目前所在元素位置
* e.target.nodeName, 來判斷當下點擊的是哪種 HTML 標籤
* e.preventDefault() - 取消預設觸發行為
```
const block2Btn = document.querySelector(".block2 .btn");
block2Btn.addEventListener("click", function(e){
    console.log("12345");
})
```

### 如何觀看 DOM 有註冊事件監聽
* chrom 檢查, 元素 右變的事件監聽器

## forEach 寫法介紹
* .forEach
* array 代表當下陣列的本身
* item 為 array\[ index \]
```
let data = [1,2,3, [4,6]];
data.forEach(function(item, index, array){
    console.log(item, index, array)
})
```

## AJAX - 網路請求
* AJAX 是一種非同步 JavaScript 技術
* 跟伺服器請求時, 在本地端同一個頁面(網址)可以只更新局部網頁內容
### 什麼是網路請求？
* 本地對伺服器發出網路請求(request), 伺服器回應(response), 像是搜尋為向 google 伺服器發出網路請求
* 一般開啟網頁時請求順序(chrom 檢查 網路 可以看順序)
  * 本地 get request html
  * 伺服器 response html
  * 本地解析 html
  * 本地依照 html 結構順序再依序向伺服器 get request  js 或 img 等
* 本地端發送請求, 除了基本的網址外, 通常還會有瀏覽器與一些個人資訊(request header), 讓伺服器能夠驗證請求是否合法, 伺服器會回應 response 與 response header(告訴本地要如何解析 response 等)
* 認證的資訊會放在 header
* 傳輸的資訊會放在 body

### 網路請求種類介紹
* get -從對方(server)取得資料
* post -傳送資料給對方(server)
* contentType 常見的格式
  * text/plain ：純文字格式
  * application/json： JSON資料格式
  * application/x-www-form-urlencoded: form表單資料被編碼為key/value格式傳送到伺服器
  * multipart/form-data ： 上傳檔案，就需要使用該格

### 網頁請求狀態碼
* [常用狀態碼](https://developer.mozilla.org/zh-TW/docs/Web/HTTP/Status)

### CORS 跨網域
* postman 無法測試跨網域
* [test-cors 測試工具](https://www.test-cors.org/)
* 目標資源與當前網頁的網域（domain）、通訊協定（protocol）或通訊埠（port）只要有任一項不同，就算是不同來源。
* 假設目前使用者在：https://example.com ：
  * https://example.com/test -> 同域
  * https://m.example.com -> 網域不同
  * https://example.com:3000 -> PORT 不同
  * http://example.com -> 通訊協定不同
* 為什麼瀏覽器要這麼雞婆把跨域請求資源擋掉呢？其實這是考量到使用者的資訊安全
* 跨域請求雖然會被瀏覽器擋下來，但攔截的是回應（Response），不是請求（Request）喔！請求指定的內容仍然會完成，開發者要特別注意這點！
* 跨域保護的限制是瀏覽器的規範，只要不透過瀏覽器發送請求，自然也就不會有限制。
* 代理伺服器
  * 可以打向另外架構的中介伺服器，或是透過 nginx 做簡易的反向代理

### 各種發出網路請求的 JS 寫法種類介紹
* JavaScript 原生寫法
  * XMLHttpRequest(較傳統的寫法，寫法較冗長)
  * Fetch(較新, 注意瀏覽器支援度)
* axios 套件, 無需額外載入 JS
  * 用法比較簡潔 
  * Make XMLHttpRequests from the browser   
  * 前後端都能用 
  * Using jsDelivr CDN 

### axios-非同步觀念
* 預設是非同步, 因為 request 可能會等很久
* 如果是非同步, 在確定得到 response 後, 要有個更新畫面或資料的 finction

## 箭頭函式
### 函式陳述式與函式表達式差異
* 函式陳述式不論寫在哪, javaScript 會自動先註冊 
* 函式表達式必須先寫, 之後才能執行
```
// 函式陳述式
numA(3); // 可以執行
function numA(x){
  return x * x;
}
numA(3);
// 函式表達式
numB(3); // 不可以執行, 要先宣告 const B
const numB = function(x){
  return x * x;
}
numB(3);
```
### 箭頭函式基本寫法
```
const numC = (x) =>{
  return x * x;
}
```
### 箭頭函式再縮寫
* 如果有 return 的可以縮寫為
```
const numD = (x) => x * x;
```
* 如果只有一個 input 參數, 可再省略為
```
const numE = x => x * x;
```
* 如果沒有參數, 要保留括號
```
const numF = () => 123;
```
### 陣列 map 搭配箭頭函式寫法
```
let arry8 = [1, 2, 3]
let newArryMap8 = arry8.map(item =>item * 4)
```

## JS 非同步技巧 - Promise、Async、Await
* [參考](https://www.casper.tw/development/2020/10/16/async-await/)
### setTimeout
* setTimeout(functionRef, delay)
* setTimeout(functionRef, delay, param1, param2)
```
function callback(x){
    console.log(x);
    console.log("callBack");
}
const timeout = setTimeout(callback , 2000, 5);
```
### 建構第一個 promise
* promise 是非同步
* Promise(padding 等待)
  * 正確(resolved)->.then(data)
  * 錯誤(reject)->.catch(error)
```
const checkScore = new Promise((resolve, reject)=>{
    const score = Math.round(Math.random()*100);
    if (score>=60){
        resolve(score);
    }
    else{
        reject("不及格");
    }
})
checkScore
    .then(data=>console.log(data))
    .catch(error=>console.log(error))
```

### promise 帶參數寫法
* 搭配箭頭函式寫法
```
const checkScore = (score) =>{
    return new Promise((resolve, reject)=>{
        console.log("正在觀察是否及格");
        setTimeout(() => {
            if (score>=60){
                resolve(score);
            }
            else{
                reject("不及格");
            }
        }, 2000);
    })
}
const score = Math.round(Math.random()*100);
checkScore(score)
    .then(data=>console.log(data))
    .catch(error=>console.log(error))
```
### async、await 再升級
* async function 是讓函式內的語法同步執行
* await 是屬於一元運算子，它會直接回傳後方表達式的值；但如果是 Promise 時則會 “等待” resovle 的結果並回傳
* await 只能在 async function 中運行，所以 async/await 基本上是一體的，不會單獨出現。
* async/await 錯誤流程可以使用 try...catch 陳述式管理流程，將原有的程式碼直接置入於 try 流程內，當遇到例外的錯誤時則撰寫在 catch 區塊內。
```
async function getData() {
    try { // 專注在成功
        const data1 = await checkScore(60);
        console.log(data1);
        const data2 = await checkScore(10);
        console.log(data2);
    }
    catch (err) { // 專注在錯誤
        console.log('catch', err);
    }
}
getData();
```
### Promise.all 平行執行，統一列出回應資訊
* Promise.all() 是 Promise 物件下的方法，此方法可傳入一個陣列，陣列包含多個 promise，Promise.all()可以統一發出所有陣列內的請求，並取得所有回應時統一回傳，async/await 可搭配此語法得到一次性的所有回應。
* 取得結果與送出的陣列順序相同
* 有一個失敗了就進入 catch
### fetch 與 promise 的關係
* fetch 是一個設計好的 promise, 用途是送出request
```
let promise = fetch(url, [options])
```
* url是要訪問的網址
* options是一堆可選的参數，例如method、header
* 如果只有url，沒有[options]，就會預設這個請求的HTTP請求方法是GET。


## NPM 介紹
* 套件管理工具
* 跟 node.js 配合
* node.js 讓你不用瀏覽器就能執行 javaScript
### Node.js、NPM 環境安裝
* [網址](https://nodejs.org/zh-tw/download/)
* 預設路徑
  * Node.js v16.17.0 to /usr/local/bin/node
  * npm v8.15.0 to /usr/local/bin/npm
  * Make sure that /usr/local/bin is in your $PATH.
### npm 基礎指令
* 終端機 cd 到專案目錄
* 初始化專案 npm init -y
* 安裝套件：npm install 模組名稱
* 安裝全域套件：npm install 模組名稱 -g
* 更新套件：npm update 模組名稱
* 更新全域套件：npm update 模組名稱 -g
* 移除套件：npm uninstall 模組名稱
* 移除全域套件：npm uninstall 模組名稱 -g
* 安裝寫在 package.json 的套件: npm install
* 全域會安裝到 /usr/local/lib/node_modules
* 一般會安裝到專案子資料夾 node_modules, 這個不會上 git
### --save、--save-dev 指令差異
* production (上線)依賴模組： npm install 模組名稱 --save, 預設沒寫也是
* development (開發)依賴模組： npm install 模組名稱 --save-dev

### nodemon
* npm install nodemon --save-dev
* npx nodemon js/index.js

## webpack 壓縮打包工具管理
* npm install webpack webpack-cli --save-dev
### 進入點(entry)、輸出點(output) 觀念建立
* 在專案根目錄建立進入點 src 資料夾, 輸出點 dist 資料夾
* npm 的 package.json 的 script 加入 build 指令, 預設 webpack 會把 src 裡的檔案輸出到 dist 資料夾
```
"scripts": {
  "test": "echo \"Error: no test specified\" && exit 1",
  "build-dev": "webpack --mode development",
  "build": "webpack --mode production" 
}
```
* npm run build 或 npm run build-dev
### webpack.config.js 環境建立
* 在專案根目錄建立 webpack.config.js 檔案
* 如果有[多個進入點參考](https://webpack.js.org/concepts/entry-points/)

### JS 模組化 ES Module
* [參考](https://www.casper.tw/development/2020/03/25/import-export/)
* import 匯入(html 或 JS)
  * 路徑是相對路徑 
  * 載入預設匯出, import 自定名稱 from "xxx.js"
  * 載入具名匯出
    * import {fn1, fn2} from ...
    * import { fn as newFn } from ...
    * import * as 自定名稱 from ... 
* 匯出(JS)
  * 預設匯出, 每個 JS 檔唯一
  * 具名匯出, 每個 JS 檔可匯出多個

* 如果要在瀏覽器直接運行模組化(不用 webpack)，在 script標籤加上 type="module" 
```
<script type="module" src="index.js"></script>
```
* 透過 webpack 打包編譯出的 bundle.js，已將 import、export 的資訊做整合成一個檔案了。bundle.js 本身沒有再做額外 import 或 export 的動作。所以 HTML 檔引入 bundle.js 時不需加上 type="module"。
