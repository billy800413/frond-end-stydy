# 六角學園
## 使用 HTML、CSS 開發一個網站, CSS3技巧

### 不同瀏覽器不同版本對 css3 的支援度各不相同
* [參考](https://caniuse.com/), 可以知道各瀏覽器的版本支援度
* 使用 [Statcounter](https://gs.statcounter.com/) 查看各國瀏覽器趨勢
* 不同瀏覽器如果有破圖跑版問題, 可參考 [pure.css](https://purecss.io/) 的內容


### 背景圓弧效果
* border-radius: 10px;
* 完整圓型 border-radius: 50%;
* 如果希望只有左上角 border-radius: 50% 0 0 0;
* 在改變形狀後可以再外加 borber

### 單行要在 block 內置中
* text-align: center;
* line-height: block高度;

### 背景漸層效果
* background:linear-gradient(上面的顏色, 下面的顏色);
* 如果要左到右[參考](https://www.w3schools.com/css/css3_gradients.asp), background:linear-gradient(to right, blue, red);

### 陰影效果
* box-shadow: x軸數值 ｙ軸數值 柔化距離 color;
* x軸ｙ軸數值為正時往右下, 柔化距離不能為負數