# 六角學園
## 使用 HTML、CSS 開發一個網站, 網頁排版技巧 Part II

### 使用絕對定位 / 相對定位 設計版型
* 當元素的 position 設定 absolute 後，它就會往外層的元素找是否有position:relative | absolute | fixed | inherit(若繼承的是前面3個之一)的元素，若是都沒有，就會以該網頁頁面(<body>)的左上角為定位點。
* left, right, top, bottom 為距離, 可設定為負數
* 通常只會設定一個角落, 如 left 和 bottom 的組合
* 網頁元素需要重疊在一起, 常用此方法
* test1.html

### z-index
* 像圖層的概念
* 數值最好只要 1~30 
* z-index 越大代表越上方, 會蓋掉底下的
* test1.html

### 使用 float ul li 設計產品列表
* test2.html
* 以目前來說大多會使用比較新的 flexbox 來做排版，而 float 是比較舊的排版運用方式。 flexbox 可以做的排版很多，有 align-items, justify-content 等等，彈性是非常大的；而 float 主要只能針對 left, right 使用，比較適合用於水平佈局，其中特別會使用在[文繞圖](https://python.luka.tw/uncategorized/past/2015-11-12-css-layout-techniques-spelling-5800cb3ac0f9/)的呈現。

### 絕對定位設計優惠價
* 使用絕對定位 / 相對定位 設計版型
* test3.html

### position: fixed 技巧
* 不希望網頁滾動時消失的話, 可以使用 position: fixed
* 通常是對 body 去 fixed
* test4.html

### 名片設計
* test5.html

### 在 html 元素上載入背景圖片 background-image
*  background-repeat 在背景圖片較單一時常用, 減少載入網頁流量
*  如果圖片不夠高或寬時常與 background-color 混用
*  想要設定背景圖片大小可用 background-size
*  想要移動 background-image的位置(預設靠左上角) 可用 background-position
*  test6.html
* 設定可縮寫, 中間用空格, 斜線主要是要區隔background-position 與 background-size, 在僅有 background-position 時，可以不需要出現斜線，background-size 出現時，必定要與 background-position 與斜線同時撰寫，否則你的原始碼會失效
‘’‘
background: color image attachment repeat poition / size
或
background: color image attachment repeat poition
’‘’
* 詳細縮寫[參考](https://ithelp.ithome.com.tw/articles/10250499)

### 圖片取代文字技巧 - 以logo為例
* 以 test7.html 為例, .header .logo h1 a, 但 a 一定要打些文字, 不然瀏覽器搜尋爬文時會有問題
```
.header .logo h1 a{
	background-image: url(./img/logo.png);
	background-color: white;
	display: block; /*要設定為 block, wh設定才生效*/
	height: 110px;
	width: 250px;
	text-indent: 101%; /*使用縮排讓文字超出大小*/
	overflow: hidden; /*超出去的隱藏*/
	white-space: nowrap; /*不換行, 所以全部超過出去*/
}
```
* 以上跟在 h 標籤裡面直接放個  a 標籤 與 img 標籤, 在語意上相同
```
<h1>
	<a href="index.html">
		<img src=“”xxx.png alt="Logo">
	</a>
</h1>
```

### 圖片種類介紹 ( gif、jpg、png )
* jpg 壓縮影像, 沒有透明圖層
* png 可以有透明圖層, 可分為 png8(256色) 與 png24
* gif 連續的 jpg 或 png 結合成的動圖, 可以有透明圖層, 最高只支援256色