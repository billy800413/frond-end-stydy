# 六角學園
## 使用 HTML、CSS 開發一個網站, CSS 常用語法

### 基本概念
* HTML代表網頁結構, CSS代表樣式
* css除錯方式, 用 chrome 的檢查功能

### html啟用css的方法
```
<head>
    <!-- link:css -->
    <link rel="stylesheet" href="css副檔名的檔案路徑">
</head>
```

### css 基本內容
* 注意大括弧與冒號與分號
```
選擇器{
    屬性: 設定值;
    屬性: 設定值;
}
```

* 如果有多個標籤都要用同一個設定, 可用逗號
```
完整表示選擇器1, 完整表示選擇器2{
    屬性: 設定值;
    屬性: 設定值;
}
```
* 如果底下可能有多層, 但我們只要設定第一層, 前面加個大於符號
```
/*如 menu 是個 ul, 底下還有個 subMenu, subMenu 裡面也有 li*/
.block8 .menu >li
```

### 常見屬性
* color: 顏色;
* font-family: 字形
* font-size: 字體大小;
* line-height: 行距;
* text-align: 文字對齊;
* text-indent: 第一行縮排;
* text-decoration: 底線中間線等;
* letter-spacing: 字母之間的間隔大小;
* border: 線條粗細 線條樣式 顏色;

### border 屬性備註
* border 代表四周
* border-left 代表只有左邊
* solid 實心線
* dashed 虛線
* dotted 圓點虛線
* border 也可用於圖片

### 標籤選擇器(html標籤)
* 設定html內的全域
* html標籤包含 h1, h2, p, a, img...

### 擬態選擇器
* 與滑鼠游標互動時一些簡單客製化的效果
* hover, 當滑鼠移動到上面時
* active, 滑鼠按著沒有放開

樣板
```
html標籤:擬態{
    屬性: 設定值;
    屬性: 設定值;
}
```

範例
```
a:hover{
    color: crimson;
    font-size: 40px;
}
```

### 類別選擇器
* 如有重複會覆蓋掉全域設定
* 使用方式為html標籤的class屬性, style1為自定義名稱

html
```
<p class="style1">客製化段落</p>
```

css
```
.style1{
    屬性: 設定值;
}
```

### 鼠標移動到上面時, 鼠標效果
* cursor: pointer;

## 問題 
```
Q: 要設定兩種不同的 a:hover 於兩個連結要如何?
A: 設定不同的class, 如在 style2 加上 hover

.style2:hover{
    color: pink;
}
```
  