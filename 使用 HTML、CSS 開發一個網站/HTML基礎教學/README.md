# 六角學園
## 使用 HTML、CSS 開發一個網站, HTML 基礎教學


* index 網頁的首頁
* 作業提交(帳號為facebook)
https://codepen.io/accounts/signup/user/free



### 快捷鍵 
* 空白html副檔名文件 輸入 ！, 啟用Emmet模板
* h1*3, 快速產生3個標籤
* p*4, 快速產生4個段落
* lorem10, 隨機產生10個英文單字

### html 文件開頭
```
<!--  指的是HTML5 -->
<!DOCTYPE html>
<html lang="en">
```

### html head
```
<!-- 舊版IE沒寫, 中文會變亂碼 -->
<meta charset="UTF-8">
<!-- 排版優化(手機等) -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- IE 專用 -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>網頁頁籤上顯示</title>
```

### html body
```
<!-- body裡的內容才會顯示在一般頁面上 -->
<h1>標題</h1>
<h2>次標題</h2>
<p>段落</p>
<!-- 列表, ul 實心圓, ol 數字-->
<ul>
    <li>項目一</li>
    <li>項目二</li>
</ul>
<ol>
    <li>項目一</li>
    <li>項目二</li>
</ol>
<img src="路徑或url" alt=" 圖片無法讀取或給視障人士播報軟體使用">
<a target="_blank" title="滑鼠點到時會有小的提示" href="路徑或url">網頁顯示文字</a>
<!-- target="_blank" 代表開啟新分頁 -->
信箱與電話可使用 emmet 語法 a:mail、a:tel 做設定以增加使用者體驗

```