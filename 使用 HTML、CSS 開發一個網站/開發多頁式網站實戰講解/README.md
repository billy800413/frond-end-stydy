# 六角學園
## 使用 HTML、CSS 開發一個網站, 開發多頁式網站實戰講解

### 使用 a 標籤連結到不同的 html 檔

### 注意 layout(共通版型元素)
* 製作多頁式時, 可以先考慮有哪些共通的版型佈局, 可以重複直接使用
* 可以先製作一個 laylout.html, 之後比較好複製貼上

### 保持網頁靈活彈性的技巧
* 有可能是變動的內容時, 儘量不要寫死高度, 善用 padding margin

### css 管理方法
* 先放 css resrt 和 共通的 layout 佈局在 layout.css, 其他個別的放在各自的 css
* css 中要匯入式引入其他 css 的內容
```
@import "xxx.css";
```

