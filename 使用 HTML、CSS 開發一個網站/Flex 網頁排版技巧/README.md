# 六角學園
## 使用 HTML、CSS 開發一個網站, Flex 網頁排版技巧
[Flex 六角線上模擬器](https://codepen.io/peiqun/pen/WYzzYX)

### 參考
* http://www.ruanyifeng.com/blog/2015/07/flex-grammar.html
* https://www.casper.tw/css/2020/03/08/flex-size/

### 基本用法
```
<div class="container">
    <!-- 內部 div 或其他 html 標籤-->
    <div class="itemBasic item1">
        .....
    </div>

    <!-- 內部 div 或其他 html 標籤-->
    <div class="itemBasic item2">
        .....
    </div>
</div>
```
* container 為外層(父層)
* 父層裡 "第一層" div 或其他 html 標籤為內部
* 通用設定可在itemBasic, 個別設定可在 item1, item2等
* Flex 設定於外層(父層), 且只能控制第一層內部
* 內部也可以再設定 Flex, 此時就相當於更內部的外層(父層)
```
<div class="container1">
    <!-- container1 為 container2 的父層, 在 container1 設定 Flex 只能控制 container2-->
    <div class="container2">
        <!-- container2 為 container3 的父層, 在 container2 設定 Flex 只能控制 container3-->
        <div class="container3">
            ...
        </div>
    </div>
</div>
```

* display: flex 及 display: flexbox 設定是相同的，但 display: flexbox 主要是用於適應某些瀏覽器設定（-ms-flexbox），一般來說我們還是使用 display: flex 來做設定。可以參考此[文章](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout/Backwards_Compatibility_of_Flexbox)。

### Flex 外層(父層)必備屬性 (container)
* display: flex(讓 container 裡面的 div 根據主軸方向排列)
* flex-direction: 決定flex的主軸線

```
flex-direction: row; (左到右, 預設值)
flex-direction: row-reverse; (右到左)
flex-direction: column; (上到下)
flex-direction: column-reverse; (下到上)
```

* justify-content: 主軸線的對齊方式

```
justify-content: flex-start; (從起點開始對齊, 預設值)
justify-content: flex-end; (從終點開始對齊)
justify-content: space-around; (置中留白左右留白1)
justify-content: space-evenly; (置中留白左右留白2)
justify-content: center; (置中貼齊左右留白)
justify-content: space-between; (置中留白左右貼齊)
```

* flex-wrap: 決定換行的屬性, 當內層寬度總和超過外層(父層)之設定時, 換成 n 行, n 根據當內層寬度總和決定, 行間距離可設定於內層的margin-bottom

```
flex-wrap: wrap;
flex-wrap: nowrap; (預設, 不換行)
```

* align-items: 交錯軸線的對齊
```
flex-direction: row; 或 flex-direction: row-reverse; 交錯軸都是上到下
flex-direction: column; 或 flex-direction: column-reverse; 交錯軸都是左到右
align-items: flex-start; (靠上或靠左)
align-items: flex-end; (靠下或靠右)
align-items: center; (置中於主軸線)
align-items: stretch; (滿版)
```

### Flex 外層(父層)常用屬性 (container)
* width: 500px;
* margin: 0 auto; (container 於網頁置中)
* padding: 10px;

### Flex 效果
* display: flex(讓 container 裡面的 div 根據主軸方向排列)
* container 裡面的每個 div 寬度, 會依照 container 的寬度自適應伸縮
* container 裡面的 div 預設自動等高, 但還是可以個別設定