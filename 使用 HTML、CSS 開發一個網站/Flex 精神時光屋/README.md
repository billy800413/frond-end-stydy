# 六角學園
## 使用 HTML、CSS 開發一個網站, Flex 精神時光屋

### 小技巧
* 全部標籤都顯示外框, 跟 border 不一樣在於, outline 不佔據實際寬度
```
*{
    outline: 1px solid black;
}
```

* a 標籤可設定 display: block; 讓 a 標籤自適應父層寬度
```
item 為 a 標籤的父層
.wrap .header .menu .item{
    width: 80px;
    text-align: center;
}

.wrap .header .menu .item a{
    background: green;
    color: white;
    display: block;
}
```

### codePen
1. [並排卡片](https://codepen.io/ffquqozd-the-reactor/pen/GRxbYBQ)
2. [雙欄排版](https://codepen.io/ffquqozd-the-reactor/pen/vYRqQER)
3. [雙欄選單設計](https://codepen.io/ffquqozd-the-reactor/pen/WNzqPLP)
4. [表頭表尾設計](https://codepen.io/ffquqozd-the-reactor/pen/RwMzdqv)
5. [常見三種排版](https://codepen.io/ffquqozd-the-reactor/pen/oNqrOmN)
6. [圖文並排](https://codepen.io/ffquqozd-the-reactor/pen/qBoeRNj)
7. [首頁橫幅排版](https://codepen.io/ffquqozd-the-reactor/pen/JjLgEry)
8. [圖文並排設計](https://codepen.io/ffquqozd-the-reactor/pen/OJvKWoY)
9. [訂單明細](https://codepen.io/ffquqozd-the-reactor/pen/oNqKZjQ)
10. [產品列表](https://codepen.io/ffquqozd-the-reactor/pen/PoRMpEZ)