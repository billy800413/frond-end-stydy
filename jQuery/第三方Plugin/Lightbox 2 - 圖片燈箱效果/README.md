# 六角學園
## jQuery-第三方Plugin-Lightbox 2 - 圖片燈箱效果

* [插件下載](https://lokeshdhakar.com/projects/lightbox2/)
* a 標籤 href 放大圖, a 標籤內容放 img 標籤, img 標籤 src 為小圖, 這樣在沒點開時只會有小圖的網路流量
```
<a href="big-img" data-lightbox="roadtrip">
    <img src="small-img" alt="">
</a>
```
* 客製化的放在 all.js



