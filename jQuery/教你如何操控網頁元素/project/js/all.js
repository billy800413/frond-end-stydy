$(document).ready(function () {
    $(".block1 .menu li").click(function (e) { 
        e.preventDefault();
        $(this).toggleClass("active");
        $(this).siblings().removeClass("active");
    });

    $(".block2 .menu .addCart").click(function (e) { 
        e.preventDefault();
        $(this).toggleClass("active");
        $(this).parent().siblings().find(".addCart").removeClass("active");
        $(this).parent().toggleClass("active");
        $(this).parent().siblings().removeClass("active");
    });

    $(".block3 .qa h3").click(function (e) { 
        e.preventDefault();
        $(this).toggleClass("active");
        $(this).parent().find("p").slideToggle();
        $(this).parent().siblings().find("h3").removeClass("active");
        $(this).parent().siblings().find("p").slideUp();
    });

    $(".block4 .chapter >li").click(function (e) { 
        e.preventDefault();
        $(this).find("h1").toggleClass("active");
        $(this).find(".section").slideToggle();
        $(this).siblings().find("h1").removeClass("active");
        $(this).siblings().find(".section").slideUp();
        
    });
    
    //=============================
    //$(".block5 h2").text("這是寫在js的h2");

    // $(".block5").html 會把 ".block5" 先清空
    $(".block5").html("<h1>這是寫在js的h1</h1>");
    //==============================
    /*
    因為先執行了click, $(".block6 .box1").html("<h1>用 html 在 js</h1>") 還沒有產生, 因此對它無效
    $(".block6 h1").click(function (e) { 
        e.preventDefault();
        alert("點擊了h1");
    });
    */
    $(".block6").on("click", "h1", function (e) {
        e.preventDefault();
        alert("點擊了h1");
    });
    $(".block6 .box1").html("<h1>用 html 在 js</h1>");
    $(".block6 .box3 h1").text("用 text 在 js");
    /*
    $(".block6 .box1").html("<h1>用 html 在 js</h1>") 先產生, 因此對它有效
    $(".block6 h1").click(function (e) { 
        e.preventDefault();
        alert("點擊了h1");
    });
    */
    //==============================

    $(".block7 > table a").click(function (e) { 
        e.preventDefault();
        alert("你刪除了一橫列");
        // 第一個 parent 到 td, 再一個才到 tr
        $(this).parent().parent().remove();
    });

    $(".block8 a").click(function (e) { 
        e.preventDefault();
        $("html,body").animate({scrollTop: 0}, 700);
    });
    
    $(".block9 a[href$='.doc']").addClass("fa fa-file-word-o");
    $(".block9 a[href$='.jpg'], a[href$='.png']").addClass("fa fa-file-photo-o");
    $(".block9 a[href$='.zip']").addClass("fa fa-file-zip-o");
});
