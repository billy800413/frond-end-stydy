# 六角學園
## jQuery

## jQuery-教你如何操控網頁元素

### this - 教你如何操作本身元素
* this 前後不用加引號
* 本身元素(目前觸發事件的), 其他同名的不算
* project block1

### parent() - 找到父階層元素
* project block2
```
$(this).parent().toggleClass("active");
```

### .siblings() - 自己以外的同層元素
* 自己以外的同層
* project block2

### find () - 輕鬆找到子元素內容
* 自己的下層
* project block3
* 在 jQuery中可以先透過 .parent() 選取到外層元素再用 .siblings() 搭配 .find("標籤") 選取特定的內元素

### 折疊選單設計
* project block3, block4

### 使用 html()、text() 載入內容
* project block5
* 使用 jQuery 動態加入
* html() 會先把那層清空再寫入
* .html 是 插入「HTML 元素」，.text() 只有單純插入文字字串

```
會把 ".block5" 先清空
$(".block5").html("<h1>這是寫在js的h1</h1>");


$(".block5 h2").text("這是寫在js的h2");
```
* 備註, 混用是可以的，但要做調整避免原有 HTML 結構被吃掉，建議將 h1 的設置移到最前面，才不會蓋到前面設置的語法，並且在其後方加入 <h2></h2> 標籤。如下：
```
$(".block5").html("<h1>這是寫在js的h1</h1><h2></h2>");
$(".block5 h2").text("這是寫在js的h2");
```

### click()、on() 的差別
* project block6
* on 會幫你去監聽目前 html 的內容(包含目前動態載入)
* click 會看 js 寫的順序

## jQuery 小技巧

###  attr() - 動態增加 HTML 標籤屬性
* 注意是修改 html 標籤屬性, 不是 css
```
$(selector).attr(attributeName, value);
```

### remove() - 移除網頁標籤小工具
* project block7
* 從瀏覽器頁面直接移除 html 標籤(跟裡面的所有內層), 跟 hide 隱藏(只更改 css )起來不同
* 常用於購物車, 刪除商品用
```
$(selector).remove();
```

### top 滑動效果
* project block8
```
$(".block8 a").click(function (e) { 
    e.preventDefault();
    $("html,body").animate({scrollTop: 0}, 700);
});
```

### 使用 fontAwesome 動態加入連結 icon
* [jQuery 線上辭典](https://oscarotero.com/jquery/), Attribute Ends With Selector name$=”value”, 來去選擇所有 a 標籤且 href 的內容包含 .doc
```
$(".block9 a[href$='.jpg'], a[href$='.png']").addClass("fa fa-file-photo-o");
```
* [fontawesome v4](https://fontawesome.com/v4/get-started/)需要 css font 資料夾與檔案
* fontawesome 是字行檔, 因此可以決定顏色大小等
* 不同的 icon [查詢](https://fontawesome.com/v4/icons/)
* 會多一個 before 在內容
* project block9
* 其他版本與[收尋頁面](https://fontawesome.com/icons?d=gallery&q=)
```
5.15.4：
CDN：<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css'/>

v6.1.1：
CDN：<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css'/>
```







