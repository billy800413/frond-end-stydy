$(document).ready(function(){
    $(".block1 .btn").click(function (e) { 
        e.preventDefault();
        $(".block1 .test, .block1 p, .block1 h2").toggle();
    });

    $(".block2 .messageBtn").click(function (e) { 
        e.preventDefault();
        $(".block2 .text").fadeToggle(3000);
    });

    $(".block3 .btn").click(function (e) { 
        e.preventDefault();
        // 注意最後一個不用加點
        $(".block3 form.text").toggleClass("active");
    });

    $(".block5 .btn").click(function (e) { 
        e.preventDefault();
        $(".block5 .box").slideUp(3000).fadeIn(3000);
        
    });

    $(".block6 a").click(function (e) { 
        e.preventDefault();
        $(".block6 .box").slideUp(3000).fadeIn(3000);
    });

    $(".block7 a").click(function (e) { 
        e.preventDefault();
        $(".block7 .box").css("width", "500px");
    });

    $(".block8 .menu .dropDown1").click(function (e) { 
        e.preventDefault();
        $(".block8 .subMenu1").slideToggle(300);
        $(".block8 .menu .dropDown1").toggleClass("active");
        $(".block8 .menu .dropDown2").removeClass("active");
        $(".block8 .subMenu2").hide(300);
    });
    $(".block8 .menu .dropDown2").click(function (e) { 
        e.preventDefault();
        $(".block8 .subMenu2").slideToggle(300);
        $(".block8 .menu .dropDown2").toggleClass("active");
        $(".block8 .menu .dropDown1").removeClass("active");
        $(".block8 .subMenu1").hide();
    });

    $(".block9 .btn").click(function (e) { 
        e.preventDefault();
        $(".block9 .box1").slideToggle(3000);
        $(".block9 .box2").delay(3000).toggle(0);
        $(".block9 .box3").slideToggle(3000);
    });

    $(".block10 .bar input.L").click(function (e) { 
        e.preventDefault();
        $(".block10 .content p").css("font-size", "30px");
        
    });
    $(".block10 .bar input.M").click(function (e) { 
        e.preventDefault();
        $(".block10 .content p").css("font-size", "20px");
    });
    $(".block10 .bar input.S").click(function (e) { 
        e.preventDefault();
        $(".block10 .content p").css("font-size", "14px");
        
    });

    $(".block11 .adClose").click(function (e) { 
        e.preventDefault();
        $(".block11 .ad").hide();
    });
    $(".block11 .adOpen").click(function (e) { 
        e.preventDefault();
        $(".block11 .ad").show();
    });

    $(".block12 .header").click(function (e) { 
        e.preventDefault();
        $(".block12 .aside").toggleClass("asideOpen");
        
    });

    $(".block13 h1").click(function (e) { 
        e.preventDefault();
        $(".block13 h1").addClass("animate__animated animate__bounce");
        $('.block13 h1').on('animationend', function() { // 監聽當動畫結束後移除 class
            $(this).removeClass('animate__animated animate__bounce');
          });
    });


});