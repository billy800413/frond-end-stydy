# 六角學園
## jQuery

### jQuery 入門
* 底層還是 javaScript, 等於是個強大的 javaScript 函式庫, 各種瀏覽器相容性也都處理好了
* [jQuery 官網]("https://jquery.com/")
* jQuery 版本請[查看](https://jquery.com/browser-support/), 不同jQuery 版本支援的瀏覽器版本可能不同
* 下載壓縮後的版本 compressed, production jQuery 3.6.1
* html 要載入兩個以上的 script, 第一個一定是 jquery(先載入核心), 接下來是自定義的 js
```
<script src="jquery-3.6.1.min.js"></script>
<script src="js/all.js"></script>
```
* 自定義的 js 內文一定是
```
$(document).ready(function(){
	// $(document).ready 的意思是先讓瀏覽器載入 jquery 並執行完成
    // 單行註解
	/*
		所有自己的程式碼打在這
	*/
});
```
* css 放在 head, js 可以放在 head css 後或 body 結尾前, 有些 js 第三方插件會要求一定要放在 body> 結尾前, 懶得記可以 js 統一放在 body 結尾前
```
  第一個放 jQuery
  <script src="js/jquery-3.6.1.min.js"></script>
  
  接下來放第三方插件
  <script src="js/lightbox-plus-jquery.min.js"></script>
  ...

  最後放客製化的 all.js 
  <script src="js/all.js"></script>
</body>
```
* CSS 檔案的載入順序要調整成先載入第三方套件、CSS Reset、最後才是自己寫的 CSS
* 在 chrome 檢查 Console 可以除錯 js, 可以在 source 更改程式碼做些快速小測試與除錯, 但要記住瀏覽器上改程式碼在本地端並沒有真正被修改, 在瀏覽器上能直接修改測試的部分只有事件類型
* [jQuery 線上辭典](https://oscarotero.com/jquery/)

### 關於套件使用，大家是使用 CDN？還是下載到本機端的比例比較高呢？
* 載入 CDN 的方式可以加速網頁載入的速度，但因為是使用外部資源，所以較無法針對內容客製化，只能使用覆蓋的方式。 但如果是使用 npm 的方式，將套件載入或直接將檔案下載到本機端的專案中，有需要時可以直接客製內容，將套件安裝在自己的專案中，也較不會有 CDN 失效導致網頁無法正常運作的風險。


### 選擇器與事件：開始讓你的網頁具有互動性吧！
* 引號內用法跟 css 一樣, 只是要有單或雙引號, 注意 id 前面有井字號,  class前面有點
```
$('標籤或 #id 或 .class').function();
$('.testClass').hide();
$('h1').hide();
$('#testId').hide();
```
* 按鈕點擊事件範例
```
$(".btn").click(function (e) { 
	e.preventDefault();
	$(".test, p").toggle();
});
```

### 常用動畫效果
* hide: 隱藏
* show: 顯示
* toggle: 自動切換 hide, show
* slideDown: 從隱藏狀態, 從上到下顯示
* slideUp: 從顯示狀態, 從下到上隱藏
* slideToggle: 自動切換 slideDown, slideUp
* fadeIn: 從隱藏狀態, 慢慢顯示
* fadeOut: 從顯示狀態, 慢慢淡出到消失
* fadeToggle: 自動切換 fadeIn, fadeOut

### 使用 toggleClass、搭配 CSS3 Transition 自訂 CSS 效果吧
* addClass, 事件觸發時在 html 動態加入 class 的名稱
* removeClass, 事件觸發時在 html 動態移除 class 的名稱
* toggleClass, 事件觸發時在 html 動態加入或移除 class 的名稱, 達到切換 css 的效果
```
html
<input class="btn3" type="button" value="打開">
<form class="text3 active3" action="index.html">
	<textarea name="" id="" cols="30" rows="10"></textarea>
	<input type="submit" value="送出">
</form>

css
.text3{
    /* opacity 為透明度, all 代表所有元素*/
    opacity: 0;
    transition: all 3s;
}
.active3{
    opacity: 1;
}

js
// 注意最後一個不用加點
$(".btn3").click(function (e) { 
	e.preventDefault();
	$(".text3").toggleClass("active3");
});
```

### 設定 CSS 動畫小密技：overflow、CSS3 transition
* 一開始設定標籤超過範圍, 並使用 overflow: hidden; 先隱藏超出範圍, 在用 transition 當某個狀態改變或事件觸發時將標籤位置移入
* 想將繁雜的資訊先隱藏, 事件觸發時再顯示時常用
```
html
<div class="box4">
	<div class="title">標題</div>
</div>

css
.box4{
    width: 200px;
    height: 100px;
    border: 2px solid black;
    overflow: hidden;
    position: relative;
}
.box4 .title{
    transition: all 1s;
    background: blue;
    position: absolute;
    bottom: -30px;
}
.box4:hover .title{
    bottom: 0px;
}
```

### jQuery 鏈式寫法
* 用點連接
```
$(".block5 .btn5").click(function (e) { 
	e.preventDefault();
	$(".block5 .box5").slideUp(3000).fadeIn(3000);
	
});
```

### 使用 preventDefault() 取消默認行為
* 如果 a 標籤加入這個, 點了連結也不會自動跳轉, 常用於 a 標籤取代一般按鈕做觸發功能
```
js
$(".block6 a").click(function (e) { 
    e.preventDefault();
    $(".block6 .box6").slideUp(3000).fadeIn(3000);
});
```

### css() - 動態載入 CSS style 設定
* 觸發後權重相當於 html 寫 style, 權重很大
```
$(".block7 a").click(function (e) { 
    e.preventDefault();
    $(".block7 .box7").css("width", "500px");
});
```
* jquery裡，css可取該標籤定義的css的屬性，attr只能取該標籤裡自帶的屬性。也就是說，如果直接在div中定義的屬性，css無法獲得。attr可以獲得css中的也可以獲得其他標籤裡自帶的屬性，而標籤裡沒有自帶的屬性，attr也無法獲得。例如：在div裡，沒有width這個屬性，所以不能用attr給他賦值，在div的css樣式裡是有width這個屬性的，所以可以用.css("width","180")來賦值。

### 設計下拉式收闔選單
* project.block8
* .block8 .menu a:hover, .block8 .menu a.active
  * a.active 代表 a 標籤且 active 為 class 名稱之一

### 延遲動畫效果的好用語法
* project.block9
* 用 delay 控制延遲, 達成動畫先後順序
* show, hide, toggle 搭配 delay, 記得要改成 show(0), hide(0), toggle(0), 不然前面的 delay 沒有效果

### 即時放大縮小字型好簡單
* project.block10
```
$(".block10 .content p").css("font-size", "14px");
```

### fixed 固定網頁內容
* project.block11
* 常用於廣告等, 設定完position: fixed; 內層 position: absolute;
* 記得使用者體驗, 可以設定開關

### 使用 stop() 讓你的動畫效果更滑順
* 即時中斷動畫, 跑另一個動畫效果
* 通常用在單一個 toggle 類型
* 如果連續多種動畫的 stop 最後的狀態可能不是我們要的
```
$(".block .box").stop().slideToggle(3000);
```

### 設定offcanvas左右選單切換
* project block12

### Animate.css - 載入第三方插件 - 增添網頁動畫豐富度
* [網址](https://animate.style/)
* 載入 css
```  
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.0/animate.min.css'/>
```
* project block13, 注意動畫後監聽動畫結束並移除 class, 讓之後可以重複觸發動畫
```
$(".block13 h1").click(function (e) { 
    e.preventDefault();
    $(".block13 h1").addClass("animate__animated animate__bounce");
    $('.block13 h1').on('animationend', function() { // 監聽當動畫結束後移除 class
        $(this).removeClass('animate__animated animate__bounce');
        });
});
```

* 使用者在瀏覽網頁時會觸發很多事件 (events) 的發生，例如按下滑鼠是一種事件、按下鍵盤按鍵是一種事件、網頁或圖片完成下載時是一種事件、表單欄位值被改變是一種事件..等等。
* 在這邊 event 全名叫做「event handlers」也就是事件處理器，而它會記錄使用者的所有行為，因此同學只需要先知道它會紀錄你點擊這個事件時的動作，例如在哪裡點，點了什麼元素等。
*  [參考](https://www.fooish.com/javascript/dom/event.html)
