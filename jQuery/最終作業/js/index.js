$(document).ready(function () {

    const swiper = new Swiper('.swiper', {
        // Optional parameters
        direction: 'horizontal',
        loop: true,
        effect: "fade",
        speed: 1000,
        
        autoplay: {
          delay: 5000,
        },
      
        // If we need pagination
        pagination: {
          el: '.swiper-pagination',
        },
      
        // Navigation arrows
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
      });
    
    $(".topbar .menu li >a[href$='#']").click(function (e) { 
        e.preventDefault();
        $(this).toggleClass("active");
        $(this).parent().siblings().find("a").removeClass("active");
        $(this).siblings().slideToggle(1000);
        $(this).parent().siblings().find(".subMenu").slideUp(1500);
        
    });

    $(".scrollTop").click(function (e) { 
        e.preventDefault();
        $("html,body").animate({scrollTop: 0}, 700);
    });
});