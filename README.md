# 六角學園的課程筆記

## Install VS Code extension
* Preview on Web Server
* Markdown All in One
* Chinese(Traditional) Language Pack
* Draw.io Intergration
* Sass
* Live Sass Compiler, [教學](https://medium.com/@enshenghuang/%E4%BD%BF%E7%94%A8vscode%E5%A4%96%E6%8E%9B%E8%87%AA%E5%8B%95%E7%B7%A8%E8%AD%AFsass-scss-9ff768d23b48)
* jQuery Code Snippets, [這邊](https://marketplace.visualstudio.com/items?itemName=donjayamanne.jquerysnippets)可以查閱各個 snippets 的介紹

## 其他
* 設計稿的標註確認 [PxCook](https://www.fancynode.com.cn/pxcook)
* 畫設計稿 Layout [Skitch](https://evernote.com/intl/zh-tw/products/skitch)
* [線上轉換工具](https://www.vectorizer.io/)
* [線上壓縮圖片工具](https://tinypng.com/)
* [mac 離線壓縮工具](https://imageoptim.com/mac)
* 查訊常用手機平板解析度
  * [Viewport Sizes](http://viewportsizes.mattstow.com/)
  * [screensiz.es](https://screensiz.es/)
  * [yesviz](https://yesviz.com/)
* google 分析 [GA](https://israynotarray.com/analytics/20190506/1879363764/)來替網站作分析

## 除錯
* [Chrome 網頁除錯功能大解密](https://courses.hexschool.com/courses/enrolled/673322)

## 常用套件
* [css reset](https://cdnjs.com/libraries/meyer-reset)
* [purecss](https://purecss.io/)
* [fontawesome v4](https://fontawesome.com/v4/get-started/)
  * [icon 查詢](https://fontawesome.com/v4/icons/) 
  * [其他版本與收尋頁面](https://fontawesome.com/icons?d=gallery&q=)
* [jQuery](https://jquery.com/)
  * [jQuery 線上辭典](https://oscarotero.com/jquery/) 
* [lightbox2](https://lokeshdhakar.com/projects/lightbox2/)
* [swiper](https://swiperjs.com/)

## 使用 HTML、CSS 開發一個網站
* HTML基礎教學
* CSS 常用語法
* 使用 CSS 變更 HTML 標籤特性
* Flex 網頁排版技巧
* Flex 精神時光屋
* 網頁排版技巧 Part II
* 表格與表單設計技巧
* CSS3技巧
* 最後，讓你的網頁變得更專業
* 開發多頁式網站實戰講解

## jQuery

## 響應式網頁設計